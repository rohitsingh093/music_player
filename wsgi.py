import os
import secrets
from datetime import datetime

from flask import Flask, render_template, flash, redirect, url_for, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from forms import SongForm

app = Flask(__name__)
app.config['SECRET_KEY'] = 'f98f6086de50d8c00ace7f47330ff4ca'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///music_player.db'
app.config['UPLOAD_FOLDER'] = 'static/mp3_files'
db = SQLAlchemy(app)


def save_song(song):
	"""
		This function save the file to the desired folder
	"""
	file = song
	random_hex = secrets.token_hex(8)
	_, f_ext = os.path.splitext(file.filename)
	song_fn = random_hex + f_ext
	song_path = os.path.join(app.root_path, 'static/mp3_files', song_fn)
	file.save(os.path.join(app.config['UPLOAD_FOLDER'], song_path))
	return song_fn


class Song(db.Model):
	"""
		Class represents song
	"""
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(64), nullable=False)
	artist = db.Column(db.String(100), nullable=False)
	album = db.Column(db.String(100), nullable=False)
	song = db.Column(db.String(100), nullable=False)
	created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


@app.route('/songs/upload', methods=['GET', 'POST'])
def upload_song():
	form = SongForm()
	if form.validate_on_submit():
		file = request.files['song']
		file_name = save_song(file)
		song = Song(
			title=form.title.data, 
			artist=form.artist.data, 
			album=form.album.data, 
			song=file_name, 
		)
		db.session.add(song)
		db.session.commit()
		flash('Song uploaded successfully', 'success')
		return redirect( url_for('list_songs') )
	return render_template('upload_song.html', title='Add Songs', form=form)

@app.route("/songs/<int:song_id>")
def get_song(song_id):
	"""
		Get the song from the database
	"""
	song = Song.query.get_or_404(song_id)
	return render_template('song.html', title=song.title, song=song)

@app.route("/songs/search", methods=['GET', 'POST'])
def list_search_song():
	"""
		Search song
	"""
	text = ''
	if request.method == "POST":
		text = request.form['text']
	page = request.args.get('page', 1, type=int)
	songs = Song.query.filter(
		or_(Song.title.startswith(text), Song.artist.startswith(text), Song.album.startswith(text))).\
	order_by(Song.created_at.desc()).\
	paginate(page=page, per_page=10)
	return render_template('list_songs.html', songs=songs, title='Songs')

@app.route('/')
@app.route('/songs')
def list_songs():
	"""
		Return list of songs with pagination
		ses.query(Table).filter(Table.fullFilePath.startswith(filePath)).all()
	"""
	page = request.args.get('page', 1, type=int)
	songs = Song.query.order_by(Song.created_at.desc()).paginate(page=page, per_page=10)
	return render_template('list_songs.html', songs=songs, title='Songs')


@app.route("/songs/<int:song_id>/delete", methods=['POST'])
def delete_song(song_id):
	"""
		Delete song from song list
	"""
	song = Song.query.get_or_404(song_id)
	db.session.delete(song)
	db.session.commit()
	flash('Your song has been deleted!', 'success')
	return redirect(url_for('list_songs'))

if '__main__' == __name__:
	app.run(debug=True)
