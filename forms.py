from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FileField
from wtforms.validators import DataRequired, Length
from flask_wtf.file import FileAllowed, FileRequired


class SongForm(FlaskForm):
	title = StringField('Title', validators=[DataRequired(), Length(min=2, max=100)])
	artist = StringField('Artist', validators=[DataRequired(), Length(min=2, max=100)])
	album = StringField('Album', validators=[DataRequired(), Length(min=2, max=100)])
	song = FileField('Song', validators=[
			DataRequired(), 
			FileRequired(),
        	FileAllowed(['mp3'], 'MP3 only!')
        ])
	submit = SubmitField('Upload')