 Music Player

Using Flask to build a music web application

Integration with Flask-WTF and Flask-SQLalchemy

### Extension:
- Forms: [Flask-WTF](https://flask-wtf.readthedocs.io/en/stable/)

- SQL ORM: [Flask-SQLalchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)


## Installation

Install with pip:

```
$ pip install -r requirements.txt
```

## DATABASE SETUP
```
$ python
from wsgi import db
db.create_all()
```

Validate music_player.db in your project repo
 
## Run Flask
```
$ python wsgi.py
```
In flask, Default port is `5000`